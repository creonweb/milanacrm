<!DOCTYPE html>
<html lang="ru">

<head>
    <base href="{'site_url' | option}" />
    <meta charset="{'modx_charset' | option}">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{$_modx->resource.pagetitle}</title>

    <meta name="theme-color" content="#000">
    <meta name="msapplication-navbutton-color" content="#000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

    <meta name="yandex-verification" content="e9d866c72b1e6047" />

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/templates/orderCRM/milana-crm.css">
</head>

<body>

<div class="bg-light mb-5">
    <div class="container">
        <!-- Image and text -->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="/docs/4.0/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
                Milana Orders
            </a>
            <div>
                <p><a href="/?action=auth/logout" data-toggle="tooltip" data-placement="top" title="Выйти из ЛК" > {$_modx->user.username}  <i class="fas fa-sign-out-alt"></i></a></p>
            </div>

        </nav>
    </div>
</div>

<div class="wrapper mt-5 mb-5">
    <div class="container">
        {$_modx->runSnippet('@FILE:snippets/orderCRM/getSearchForm.php',[
            'tplAdmin' => '@FILE:chunks/orderCRM/orders.search.form.sudo.tpl',
            'tplMgr' => '@FILE:chunks/orderCRM/orders.search.form.mgr.tpl',
        ])}
        <div class="crm__orders" data-crm-orders>
            {$_modx->resource.content}
        </div>
        <div class="crm__modal" data-crm-modal style="display: none">

        </div>
    </div>
</div>


<div class="well bg-light p-2">
<div class="container">
    <div class="row">
        <div class="col-12">
            <p>milanaOrders</p>
            <p>Система управления заказами v0.1</p>
            <p>Username: {$_modx->user.username}</p>
            <p style="color:#aaa;font-size: 10px">Разработано: <a style="color:#aaa;font-size: 10px" target="_blank" href="https://redkin.site">Redkin.site</a></p>
        </div>
    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/220424958b.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

</body>

</html>