<?php

$pdoFetch = $modx->getService('pdoFetch');
$pdoFetch->setConfig($scriptProperties);
$is_sudo = false;

if (!isset($tplAdmin)) {
    $tplAdmin = $modx->getOption('tplAdmin', $scriptProperties, '@FILE:chunks/orderCRM/orders.search.form.sudo.tpl');
}
if (!isset($tplMgr)) {
    $tplMgr = $modx->getOption('tplMgr', $scriptProperties, '@FILE:chunks/orderCRM/orders.search.form.mgr.tpl');
}
if ($modx->user->isMember('Administrator')) {
    $tplOuter = $tplAdmin;
    $is_sudo = true;
} else {
    $tplOuter = $tplMgr;
}

return $pdoFetch->getChunk($tplOuter);