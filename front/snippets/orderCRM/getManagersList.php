<?php

$managers = $modx->getCollection('modUser',[
    'primary_group' => 3
]);

foreach ($managers as $manager){
    $mgrs[] = array_merge($manager->getOne('Profile')->toArray(), $manager->toArray());
}

return $mgrs;