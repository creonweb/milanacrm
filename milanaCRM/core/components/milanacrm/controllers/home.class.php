<?php

/**
 * The home manager controller for milanaCRM.
 *
 */
class milanaCRMHomeManagerController extends modExtraManagerController
{
    /** @var milanaCRM $milanaCRM */
    public $milanaCRM;


    /**
     *
     */
    public function initialize()
    {
        $this->milanaCRM = $this->modx->getService('milanaCRM', 'milanaCRM', MODX_CORE_PATH . 'components/milanacrm/model/');
        parent::initialize();
    }


    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return ['milanacrm:default'];
    }


    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }


    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        return $this->modx->lexicon('milanacrm');
    }


    /**
     * @return void
     */
    public function loadCustomCssJs()
    {
        $this->addCss($this->milanaCRM->config['cssUrl'] . 'mgr/main.css');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/milanacrm.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/misc/utils.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/misc/combo.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/widgets/items.grid.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/widgets/items.windows.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/widgets/home.panel.js');
        $this->addJavascript($this->milanaCRM->config['jsUrl'] . 'mgr/sections/home.js');

        $this->addHtml('<script type="text/javascript">
        milanaCRM.config = ' . json_encode($this->milanaCRM->config) . ';
        milanaCRM.config.connector_url = "' . $this->milanaCRM->config['connectorUrl'] . '";
        Ext.onReady(function() {MODx.load({ xtype: "milanacrm-page-home"});});
        </script>');
    }


    /**
     * @return string
     */
    public function getTemplateFile()
    {
        $this->content .= '<div id="milanacrm-panel-home-div"></div>';

        return '';
    }
}