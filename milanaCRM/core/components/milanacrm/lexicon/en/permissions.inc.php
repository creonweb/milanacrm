<?php
/**
 * Russian permissions Lexicon Entries for milanaCRM
 *
 * @package milanaCRM
 * @subpackage lexicon
 */
$_lang['milanacrm_save'] = 'Permission for save/update data.';