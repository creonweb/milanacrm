<?php
include_once 'setting.inc.php';

$_lang['milanacrm'] = 'milanaCRM';
$_lang['milanacrm_menu_desc'] = 'Пример расширения для разработки.';
$_lang['milanacrm_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['milanacrm_items'] = 'Предметы';
$_lang['milanacrm_item_id'] = 'Id';
$_lang['milanacrm_item_name'] = 'Название';
$_lang['milanacrm_item_description'] = 'Описание';
$_lang['milanacrm_item_active'] = 'Активно';

$_lang['milanacrm_item_create'] = 'Создать предмет';
$_lang['milanacrm_item_update'] = 'Изменить Предмет';
$_lang['milanacrm_item_enable'] = 'Включить Предмет';
$_lang['milanacrm_items_enable'] = 'Включить Предметы';
$_lang['milanacrm_item_disable'] = 'Отключить Предмет';
$_lang['milanacrm_items_disable'] = 'Отключить Предметы';
$_lang['milanacrm_item_remove'] = 'Удалить Предмет';
$_lang['milanacrm_items_remove'] = 'Удалить Предметы';
$_lang['milanacrm_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['milanacrm_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['milanacrm_item_active'] = 'Включено';

$_lang['milanacrm_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['milanacrm_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['milanacrm_item_err_nf'] = 'Предмет не найден.';
$_lang['milanacrm_item_err_ns'] = 'Предмет не указан.';
$_lang['milanacrm_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['milanacrm_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['milanacrm_grid_search'] = 'Поиск';
$_lang['milanacrm_grid_actions'] = 'Действия';