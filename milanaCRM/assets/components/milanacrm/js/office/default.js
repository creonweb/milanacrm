Ext.onReady(function () {
    milanaCRM.config.connector_url = OfficeConfig.actionUrl;

    var grid = new milanaCRM.panel.Home();
    grid.render('office-milanacrm-wrapper');

    var preloader = document.getElementById('office-preloader');
    if (preloader) {
        preloader.parentNode.removeChild(preloader);
    }
});