<?php

return [
    'milanaCRM' => [
        'file' => 'milanacrm',
        'description' => 'MilanaCRM plugin handler',
        'events' => [
            'milanaCRMSetManager',
        ],
    ],
];